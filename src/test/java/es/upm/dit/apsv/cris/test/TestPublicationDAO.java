package es.upm.dit.apsv.cris.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.upm.dit.apsv.cris.dao.PublicationDAO;
import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dataset.CSV2DB;
import es.upm.dit.apsv.cris.model.Publication;

class TestPublicationDAO {

	private Publication p;
	private PublicationDAO pdao;
	       
	  @BeforeAll
	  static void dbSetUp() throws Exception {
	        CSV2DB.loadPublicationsFromCSV("publications.csv");
	        CSV2DB.loadResearchersFromCSV("researchers.csv");
	   }
	
	  @BeforeEach
	  void setUp() throws Exception {
	        pdao = PublicationDAOImplementation.getInstance();
	        p = new Publication();
	        p.setId("85019352728");
	        p.setTitle("Structural composites for multifunctional applications: Current challenges and future trends");
	        p.setPublicationName("Progress in Materials Science");
	        p.setPublicationDate("2017-08-01");
	        p.setAuthors("7403372571;23101342100;6602977547;55500167800;55708653400");
	        p.setCiteCount(0);
	  }
	  
	  @Test
	  void testCreate() {
          pdao.delete(p);
          pdao.create(p);
          assertEquals(p, pdao.read(p.getId()));
	  }

	  @Test
	  void testRead() {
          assertEquals(p, pdao.read(p.getId()));
	  }

	  @Test
	  void testUpdate() {
          int oldCiteCount = p.getCiteCount();
          p.setCiteCount(10);
          pdao.update(p);
          assertEquals(p, pdao.read(p.getId()));
          p.setCiteCount(oldCiteCount);
          pdao.update(p);
	  }

	  @Test
	  void testDelete() {
          pdao.delete(p);
          assertNull(pdao.read(p.getId()));
          pdao.create(p);
	  }

	  @Test
	  void testReadAll() {
          assertTrue(pdao.readAll().size() > 75);
	  }

}
